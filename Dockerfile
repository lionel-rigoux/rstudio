FROM rocker/tidyverse:4.3.0

RUN apt-get update -qq && \
    apt-get install -y -qq libxtst6 libxt6 

RUN apt-get update -qq && \
    echo "ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true" | debconf-set-selections && \
    apt-get install -y -qq sudo ttf-mscorefonts-installer && \
    apt-get -y purge fonts-roboto fonts-roboto-unhinted fonts-lmodern fonts-texgyre && \
    sudo fc-cache -fv && \
    # Clean up after apt.
    rm -rf /var/lib/apt/lists/*

RUN install2.r -e \
    distributional \
    extrafont \
    fmsb \
    ggdist \
    ggpubr \
    ggnewscale \
    gridExtra \
    here \
    patchwork \
    readxl \
    rsvg \
    see \
    svglite

ENV HOME=/home/rstudio
WORKDIR $HOME

RUN R -e "extrafont::font_import(prompt=F); extrafont::loadfonts(); library(tidyverse)"