# rstudio


## Setup (to do only once)

**Docker**

```sh
RSTUDIO_VERSION=4.3
docker pull registry.gitlab.com/lionel-rigoux/rstudio:$RSTUDIO_VERSION
```

**Singularity**

```sh
RSTUDIO_VERSION=4.3
singularity pull --name rstudio-$RSTUDIO_VERSION.sif docker://registry.gitlab.com/lionel-rigoux/rstudio:$RSTUDIO_VERSION

# create temp dirs to allow writable temp and var folders in the image
TMPDIR=~/rstudio$RSTUDIO_VERSION-tmp # your choice
mkdir -p "${TMPDIR}"/tmp/rstudio-server
uuidgen > "${TMPDIR}"/tmp/rstudio-server/secure-cookie-key
chmod 600 "${TMPDIR}"/tmp/rstudio-server/secure-cookie-key
mkdir -p "${TMPDIR}"/var/{lib,run} 

```

## Start RStudio

Go to the root folder of your project, start the image (see below), then open a browser and go to http://localhost:8787 and login with `rstudio`/`apassword`.
 

**Docker**

```sh
docker run -ti -p 8787:8787 -e PASSWORD=apassword -v "${PWD}":/home/rstudio registry.gitlab.com/lionel-rigoux/rstudio:$RSTUDIO_VERSION
```

**Singularity**

```sh
singularity exec \
    -B "${TMPDIR}"/var/lib:/var/lib/rstudio-server \
    -B "${TMPDIR}"/var/run:/var/run/rstudio-server \
    -B "${TMPDIR}"/tmp:/tmp \
    --home "${PWD}":/home/rstudio \
    rstudio-$RSTUDIO_VERSION.sif \
    rserver --www-address=127.0.0.1 --server-user=$USER
```


## Run R script

**Docker**

```
docker run -ti -v "${PWD}":/home/rstudio registry.gitlab.com/lionel-rigoux/rstudio:$RSTUDIO_VERSION R -e "source('relpath/to/script.R')"
```

**Singularity**

```sh
singularity exec --home "${PWD}":/home/rstudio rstudio-$RSTUDIO_VERSION.sif R -e "source('relpath/to/script.R')"
```

You could also use `Rscript pathtoscript` instead of `R -e "source('pathtoscript')"`, but this will ignore default setup (system fonts, tidyverse, theme setting, etc.).
